<?php
if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

/**
 * Transforme la chaine représentant le numéro du commit en chaine formatée pour la Zone, le Core ou la Forge git
 * le cas échéant ou renvoie la chaine fournie en entrée.
 * Pour la Zone, le Core ou la Forge git on renvoie un lien vers le log de commit associé.
 *
 * @param string $valeur Le numéro du commit sous la forme z11111, c22222, g1afeee87 ou une chaine quelconque
 * @param string $info   L'information typée mise à jour avec la valeur formatée
 *
 * @return void
 */
function inc_todo_formater_commit_dist(string $valeur, string &$info) : void {

	// Par défaut, si la valeur n'est pas exprimée dans un format reconnu, on la retourne tel que.
	$commit = $valeur;

	$url = '';
	$reference = '';
	if (preg_match('#^([zc])([0-9]+)$#Uis', $valeur, $m)) {
		if ($m[1] === 'z') {
			// Commit de la zone SVN
			$url = 'https://zone.spip.org/trac/spip-zone/changeset/' . $m[2];
		} else {
			// Commit du core
			$url = 'https://core.spip.net/projects/spip/repository/revisions/' . $m[2];
		}
		$reference = $m[2];
	} elseif (preg_match('#^g:([xstog]):([\w-]+):([0-9a-f]+)$#Uis', $valeur, $m)) {
		// Commit de la forge gitea`
		include_spip('inc/gitea_utils');
		$url = 'https://git.spip.net/' . gitea_identifier_organisation($m[1]) . '/' . $m[2] . '/commit/' . $m[3];
		$reference = substr($m[3], 0, 8);
	}

	if (
		$url
		and $reference
	) {
		$commit = '<a class="spip_out" rel="external" href="' . $url . '">' . $reference . '</a>';
	}

	// On retourne le commit calculé à la suite des précédents si nécessaire.
	$info .= !$info ? $commit : ', ' . $commit;
}
