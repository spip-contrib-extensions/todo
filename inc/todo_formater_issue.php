<?php
if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

/**
 * Transforme la chaine représentant le numéro du ticket en chaine formatée pour la Forge git
 * le cas échéant ou renvoie la chaine fournie en entrée.
 * Pour la Zone, le Core ou la Forge git on renvoie un lien vers le log de commit associé.
 *
 * @param string $valeur Le numéro du ticket
 * @param string $info   L'information typée mise à jour avec la valeur formatée
 *
 * @return void
 */
function inc_todo_formater_issue_dist(string $valeur, string &$info) : void {
	// Par défaut, si la valeur n'est pas exprimée dans un format reconnu, on la retourne tel que.
	$issue = $valeur;

	if (preg_match('#^g:([xstog]):([\w-]+):([0-9]+)$#Uis', $valeur, $m)) {
		// Issue de la forge gitea
		include_spip('inc/gitea_utils');
		$url = 'https://git.spip.net/' . gitea_identifier_organisation($m[1]) . '/' . $m[2] . '/issues/' . $m[3];
		$reference = '#' . $m[3];
		$issue = '<a class="spip_out" rel="external" href="' . $url . '">' . $reference . '</a>';
	}

	// On retourne le commit calculé à la suite des précédents si nécessaire.
	$info .= !$info ? $issue : ', ' . $issue;
}
