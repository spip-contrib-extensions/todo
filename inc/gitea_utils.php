<?php

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

/**
 * Renvoie le nom complet de l'organisation Gitea de la forge SPIP à partir de son identifiant abrégé.
 *
 * @param string $id_orga Identifiant abrégé de l'organisation, soit x, s, t, o, g.
 *
 * @return string Nom complet de l'organisation ou chaine vide si erreur.
 */
function gitea_identifier_organisation(string $id_orga) : string {
	// La liste des organisations de la forge Git
	static $orgas = [
		'x' => 'spip-contrib-extensions',
		's' => 'spip-contrib-squelettes',
		't' => 'spip-contrib-themes',
		'o' => 'spip-contrib-outils',
		'g' => 'spip-galaxie',
	];

	// On retourne le commit calculé à la suite des précédents si nécessaire.
	return $orgas[$id_orga] ?? '';
}
