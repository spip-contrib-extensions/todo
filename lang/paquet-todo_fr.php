<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// Fichier source, a modifier dans https://git.spip.net/spip-contrib-extensions/todo.git

return [

	// T
	'todo_description' => 'Ajoute de nouveaux raccourcis typographiques permettant de décrire de manière simple des listes de choses à faire dans un contenu SPIP. Cette version 2 étend la liste des statuts disponibles et permet la qualification des tâches par une priorité, des étiquettes et des informations typées comme les dates de début et de fin ou les numéros de commit et de version.',
	'todo_slogan' => 'Lister rapidement des choses à faire',
];
