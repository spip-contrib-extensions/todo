<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/todo?lang_cible=it
// ** ne pas modifier le fichier **

return [

	// L
	'label_commit' => 'Revisione',
	'label_debut' => 'Inizio',
	'label_fin' => 'Fine',
	'label_priorite' => ' !',
	'label_statut' => 'Stato',
	'label_titre' => 'Titolo',
	'label_version' => 'Versione',

	// O
	'outil_inserer_todo' => 'Inserisci un’attività da fare',
	'outil_inserer_todo_egal' => 'Inserisci un’attività interrotta',
	'outil_inserer_todo_exclamation' => 'Inserisci un’attività che richiede un’azione',
	'outil_inserer_todo_interrogation' => 'Inserisci un’attività il cui stato è sconosciuto',
	'outil_inserer_todo_moins' => 'Inserisci un’attività completata',
	'outil_inserer_todo_o' => 'Inserisci un’attività in corso',
	'outil_inserer_todo_plus' => 'Inserisci un’attività da fare',
	'outil_inserer_todo_x' => 'Inserisci un’attività abbandonata',

	// S
	'statut_abandonne' => 'Abbandonato',
	'statut_afaire' => 'Da fare',
	'statut_alerte' => 'AVVISO: richiede un’azione',
	'statut_arrete' => 'Smettere',
	'statut_encours' => 'In corso',
	'statut_inconnu' => 'AVVISO: stato sconosciuto',
	'statut_termine' => 'Terminato',

	// T
	'tri_cle' => 'Rimettere in ordine',
	'tri_commit' => 'Ordina per revisione',
	'tri_debut' => 'Ordina per data di inizio',
	'tri_fin' => 'Ordina per data di fine',
	'tri_priorite' => 'Ordina per priorità',
	'tri_statut' => 'Ordina per stato',
	'tri_titre' => 'Ordina per titolo',
	'tri_version' => 'Ordina per versione',
];
