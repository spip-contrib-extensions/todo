<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/todo?lang_cible=es
// ** ne pas modifier le fichier **

return [

	// E
	'erreur_syntaxe_tache' => 'Error de sintaxis',

	// L
	'label_commit' => 'Revisión',
	'label_debut' => 'Comienzo
',
	'label_fin' => 'Fin',
	'label_priorite' => '!',
	'label_statut' => 'Estatus',
	'label_titre' => 'Título',
	'label_version' => 'Versión',

	// O
	'outil_inserer_todo' => 'Insertar un "to-do"',
	'outil_inserer_todo_egal' => 'Insertar una tarea detenida',
	'outil_inserer_todo_exclamation' => 'Insertar una tarea que necesita una acción',
	'outil_inserer_todo_interrogation' => 'Insertar una tarea cuyo estatus es desconocido',
	'outil_inserer_todo_moins' => 'Insertar una tarea terminada',
	'outil_inserer_todo_o' => 'Insertar una tarea en curso',
	'outil_inserer_todo_plus' => 'Insertar una tarea para hacer',
	'outil_inserer_todo_x' => 'Insertar una tarea abandonada',

	// S
	'statut_abandonne' => 'Abandonado',
	'statut_afaire' => 'Para hacer',
	'statut_alerte' => 'Alerta, necesita una acción',
	'statut_arrete' => 'Detenido',
	'statut_encours' => 'En curso',
	'statut_inconnu' => 'Alerta, estatus no conocido',
	'statut_termine' => 'Terminado',

	// T
	'tri_cle' => 'Restablecer en orden',
	'tri_commit' => 'Clasificar por revisión',
	'tri_debut' => 'Clasificar por fecha de inicio',
	'tri_fin' => 'Clasificar por fecha de finalización',
	'tri_priorite' => 'Clasificar por prioridad',
	'tri_statut' => 'Clasificar por estatus',
	'tri_titre' => 'Clasificar por título',
	'tri_version' => 'Clasificar por versión',
];
