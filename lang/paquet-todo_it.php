<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/paquet-todo?lang_cible=it
// ** ne pas modifier le fichier **

return [

	// T
	'todo_description' => 'Aggiunge nuove scorciatoie tipografiche per descrivere facilmente gli elenchi di cose da fare nel contenuto SPIP. Questa versione 2 estende l’elenco degli stati disponibili e consente la qualificazione delle attività per priorità, etichette e informazioni tipizzate quali date di inizio e fine o numeri di commit e di versione.',
	'todo_slogan' => 'Elenca rapidamente le cose da fare',
];
