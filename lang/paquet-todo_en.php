<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/paquet-todo?lang_cible=en
// ** ne pas modifier le fichier **

return [

	// T
	'todo_description' => 'Adds new typographical shortcuts for describing to-do lists in SPIP content in a simple way. This version 2 extends the list of available statuses and allows tasks to be qualified by priority, labels and typed information such as start and end dates or commit and version numbers.',
	'todo_slogan' => 'Quickly list things to do',
];
