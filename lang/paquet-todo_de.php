<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/paquet-todo?lang_cible=de
// ** ne pas modifier le fichier **

return [

	// T
	'todo_description' => 'Ergänzt SPIP  um typografische Kürzel, mit denen ToDo-Listen in SPIP-Textobjekten erfaßt und angezeigt werden können. Die vorliegende Version 2 erweitert die Liste der Status-Modi und erlaubt, es Aufgaben eine Priorität zuzuweisen, sowie Bezeichnungen und Standardinformationen wie Start- und Enddatum oder Versions- und Commit-Nummern zuzuordnen.',
	'todo_slogan' => 'Aufgaben schnell auflisten',
];
