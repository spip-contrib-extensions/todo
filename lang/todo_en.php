<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/todo?lang_cible=en
// ** ne pas modifier le fichier **

return [

	// E
	'erreur_syntaxe_tache' => 'Syntax error',

	// L
	'label_commit' => 'Commit',
	'label_debut' => 'Start',
	'label_fin' => 'End',
	'label_issue' => 'Issue',
	'label_priorite' => '!',
	'label_statut' => 'Status',
	'label_titre' => 'Title',
	'label_version' => 'Version',

	// O
	'outil_inserer_todo' => 'Insert a todo list',
	'outil_inserer_todo_egal' => 'Insert an suspended task',
	'outil_inserer_todo_exclamation' => 'Insert a task requiring an action',
	'outil_inserer_todo_interrogation' => 'Insert a task with unknown status',
	'outil_inserer_todo_moins' => 'Insert a completed task',
	'outil_inserer_todo_o' => 'Insert a task in progress',
	'outil_inserer_todo_plus' => 'Insert a task to start',
	'outil_inserer_todo_x' => 'Insert an aborted task',

	// S
	'statut_abandonne' => 'Aborted',
	'statut_afaire' => 'Not started',
	'statut_alerte' => 'Alert, action required',
	'statut_arrete' => 'Suspended',
	'statut_encours' => 'In progress',
	'statut_inconnu' => 'Alert, status unknown',
	'statut_termine' => 'Completed',

	// T
	'tri_cle' => 'Put back in order',
	'tri_commit' => 'Sort by commit',
	'tri_debut' => 'Sort by start date',
	'tri_fin' => 'Sort by end date',
	'tri_priorite' => 'Sort by priority',
	'tri_statut' => 'Sort by status',
	'tri_titre' => 'Sort by title',
	'tri_version' => 'Sort by version',
];
