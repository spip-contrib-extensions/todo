<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/paquet-todo?lang_cible=ru
// ** ne pas modifier le fichier **

return [

	// T
	'todo_description' => 'Добавляет новые символы для форматирования текста, которыми удобно составлять списки дел и задачю', # MODIF
	'todo_slogan' => 'Новые элементы форматирования текста в редакторе',
];
